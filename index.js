const express = require('express')
const app = express()
const cors = require('cors')
require('dotenv').config()
const mongoose = require('mongoose')
const port = process.env.PORT
const db = process.env.mongoDB

const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

mongoose.connect(db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

mongoose.connection.on('error', () => {
  console.error("Error! Can't connect to our Mongo Atlas DB")
})

mongoose.connection.once('open', () => {
  console.log('Connected to Mongo Cloud DB')
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoutes = require('./routes/user')
app.use('/api/users', cors(corsOptions), userRoutes)

const recordRoutes = require('./routes/record')
app.use('/api/records', cors(corsOptions), recordRoutes)

const categoryRoutes = require('./routes/category')
app.use('/api/categories', cors(corsOptions), categoryRoutes)

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
