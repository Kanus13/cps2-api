const Category = require('../models/category')
const User = require('../models/user')
require('dotenv').config()
const request = require('request')

module.exports.getAll = (params) => {
//controller for retrieving all records
return User.findById(params.userId)
  .then((user, error) => {
    if(error) return false
    const categoryIds = user.categories.map(category => {
      return category.categoryId
    })
    console.log(categoryIds);
    return Category.find().where('_id').in(categoryIds).then(categories => {
      console.log(categories.filter(category => category.isActive === true));
      return categories.filter(category => category.isActive === true)
    })
  })
}

module.exports.getOne = (params) => {
  return Category.findById(params.id).then((category, err) => {
    if (err) return false
    return category
  })
}

//controller for creating a category (only registered users may do so)
module.exports.add = (params) => {
  if(params.user.isRegistered){
    const category = new Category({
      name: params.name,
      type: params.type   
    })
    return category.save().then((category, error) => {
      console.log(category);
      if(error) return false
        return User.findById(params.user.id).then((user, error) => {
          if(error) return false
            user.categories.push({categoryId: category._id})

          return user.save().then((user, error) => {
            if(error) return false
              return true
          })
        })
    })
  }
}

//controller for allowing an registered user to update a record
module.exports.update = (params) => {
  //determine if user is authorized to perform update
  if (params.isRegistered) {
    return Category.findById(params.categoryId).then((category, err) => {
      //   console.log(record)
      if (err) return false
      category.name = params.name
      category.type = params.type
      return category.save().then((updatedCategory, err) => {
        // console.log(updatedRecord)
        return err ? false : true
      })
    })
  } else {
    return false
  }
}
