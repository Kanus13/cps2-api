const User = require('../models/user')
const Record = require('../models/record')
const Category = require('../models/category')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
require('dotenv').config()
const clientId = process.env.clientId
const request = require('request')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

module.exports.register = (params) => {
  const user = new User({
    firstName: params.firstName,
    lastName: params.lastName,
    mobileNumber: params.mobileNumber,
    email: params.email,
    password: bcrypt.hashSync(params.password, 10),
    loginType: 'email',
  })
  return user.save().then((user, err) => {
    if (user !== null) {
      const shortCode = process.env.shortCode
      const accessToken = process.env.accessToken
      const clientCorrelator = process.env.clientCorrelator
      const mobileNumber = user.mobileNumber
      const message = `You have successfully registered in our Budget Tracker App. Thank you and have a nice day ahead ${user.firstName} ${user.lastName}.`

      const options = {
        method: 'POST',
        url:
          'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' +
          shortCode +
          '/requests',
        qs: { access_token: accessToken },
        headers: { 'Content-Type': 'application/json' },
        body: {
          outboundSMSMessageRequest: {
            clientCorrelator: clientCorrelator,
            senderAddress: shortCode,
            outboundSMSTextMessage: { message: message },
            address: mobileNumber,
          },
        },
        json: true,
      }

      request(options, (error, res, body) => {
        if (error) throw new Error(error)
        console.log(`SMS sent to ${user.mobileNumber}`)
      })
      const msg = {
        to: user.email,
        from: 'belgachris83@gmail.com',
        subject: 'Magastos App',
        text: 'You are now registered.',
        html:
          '<strong>Thank you for taking the time to register. Have a great day ahead!</strong>',
      }
      sgMail
        .send(msg)
        .then(() => console.log(`sending of email to ${user.email} succeeded`))
        .catch(() => console.log(`sending of email to ${user.email} failed`))
    }
    return err ? false : true
  })
}


module.exports.login = (params) => {
  return User.findOne({ email: params.email }).then((user) => {
    if (user === null) {
      return false
    }
    const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

    if (isPasswordMatched) {
      return {
        accessToken: auth.createAccessToken(user.toObject()),
      }
    } else {
      //hashed passwords did NOT match
      return false
    }
  })
}

module.exports.getDetails = (params) => {
  return User.findById(params.userId).then((user, err) => {
    if (err) return false
    user.password = undefined
    return user
  })
}

module.exports.getAllIds = () => {
  return User.find().then((users, err) => {
    if (err) return false
    const ids = users.map((user) => user._id)
    return ids
  })
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
  const client = new OAuth2Client(clientId)
  const data = await client.verifyIdToken({
    idToken: tokenId,
    audience: clientId,
  })

  if (data.payload.email_verified === true) {
    const user = await User.findOne({ email: data.payload.email }).exec()
    if (user !== null) {
      if (user.loginType === 'google') {
        return { accessToken: auth.createAccessToken(user.toObject()) }
      } else {
        return { error: 'login-type-error' }
      }
    } else {
      const newUser = new User({
        firstName: data.payload.given_name,
        lastName: data.payload.family_name,
        email: data.payload.email,
        loginType: 'google',
      })

      return newUser.save().then((user, err) => {
        console.log(user)
        return { accessToken: auth.createAccessToken(user.toObject()) }
      })
    }
  } else {
    return { error: 'google-auth-error' }
  }
}

module.exports.activeRecords = (params) => {
  //console.log(params.userId);
  return User.findById(params.userId).then((user, error) => {
    
      if (error) return false
      const recordIds = user.records.map(record => record.recordId)
      //console.log(recordIds);
      return Record.find().where('_id').in(recordIds).then((records, error) => {
        console.log(records);
          if (error) return false
          return records.filter(record => record.isActive === true)
      })
  })
}

module.exports.activeCategories = (params) => {
  //console.log(params.userId);
  return User.findById(params.userId).then((user, error) => {
    
      if (error) return false
      const categoryIds = user.categories.map(category => category.categoryId)
      //console.log(recordIds);
      return Category.find().where('_id').in(categoryIds).then((categories, error) => {
        console.log(categories);
          if (error) return false
          return categories.filter(category => category.isActive === true)
      })
  })
}