const Record = require('../models/record')
const User = require('../models/user')
require('dotenv').config()
const request = require('request')

//controller for retrieving all records
module.exports.getAll = (params) => {
  //modify to instead return all records regardless of active status
  return User.findById(params.userId)
  .then((user, error) => {
    if(error) return false
    const recordIds = user.records.map(record => {
      return record.recordId
    })
    console.log(recordIds);
    return Record.find().where('_id').in(recordIds).then(records => {
      console.log(records.filter(record => record.isActive === true));
      return records.filter(record => record.isActive === true)
    })
  })
}

module.exports.getOne = (params) => {
  return Record.findById(params.id).then((record, err) => {
    if (err) return false
      return record
  })
}

//controller for creating a record (only registered users may do so)
module.exports.add = (params) => {
  if(params.user.isRegistered){
    const record = new Record({
      name: params.name,
      type: params.type,
      amount: params.amount,
      description: params.description,   
    })
    return record.save().then((record, error) => {
      console.log(record);
      if(error) return false
        return User.findById(params.user.id).then((user, error) => {
          if(error) return false
            user.records.push({recordId: record._id})

          return user.save().then((user, error) => {
            if(error) return false
              return true
          })
        })
    })
  }
}

//controller for allowing an registered user to update a record
module.exports.update = (params) => {
  //determine if user is authorized to perform update
  if (params.isRegistered) {
    return Record.findById(params.recordId).then((record, err) => {
      //   console.log(record)
      if (err) return false
        record.name = params.name
      record.type = params.type
      record.amount = params.amount
      record.description = params.description

      return record.save().then((updatedRecord, err) => {
        // console.log(updatedRecord)
        return err ? false : true
      })
    })
  } else {
    return false
  }
}

module.exports.archive = (params) => {
    if(params.isRegistered){
        return Record.findById(params.recordId)
        .then((record, err) => {
            if(err) return false
            //toggle course availability
            if(record.isActive === true){
                record.isActive = false
            }else{
                record.isActive = true
            }
            return record.save()
            .then((updatedRecord, err) => {
                return err ? false : true
            })
        })
    }else{
        return false
    }
}