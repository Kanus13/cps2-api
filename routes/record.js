const express = require('express')
const router = express.Router()
const auth = require('../auth')
const RecordController = require('../controllers/record')

router.get('/user/:userId', (req, res) => {
  RecordController.getAll(req.params).then((records) => res.send(records))
})

router.get('/:id', (req, res) => {
  RecordController.getOne({ id: req.params.id }).then((record) =>
    res.send(record)
  )
})

router.post('/', auth.verify, (req, res) => {
  const arg = {
    user: auth.decode(req.headers.authorization),
    name: req.body.name,
    type: req.body.type,
    amount: req.body.amount,
    description: req.body.description,
  }
  RecordController.add(arg).then((result) => res.send(result))
})

router.put('/:id', auth.verify, (req, res) => {
  //obtain the URL parameter via req.params
  //identify whether the user who sent this request is AUTHORIZED to do so
  const arg = {
    recordId: req.params.id,
    name: req.body.name,
    type: req.body.type,
    amount: req.body.amount,
    description: req.body.description,
    isRegistered: auth.decode(req.headers.authorization).isRegistered,
  }
  RecordController.update(arg).then((result) => res.send(result))
})

router.delete('/:id', auth.verify, (req, res) => {
  const arg = {
    recordId: req.params.id,
    isRegistered: auth.decode(req.headers.authorization).isRegistered,
  }
  RecordController.archive(arg).then((result) => res.send(result))
})

module.exports = router
