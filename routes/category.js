const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CategoryController = require('../controllers/category')

router.get('/user/:userId', (req, res) => {
  CategoryController.getAll(req.params).then((categories) => res.send(categories))
})

router.get('/:id', (req, res) => {
  CategoryController.getOne({ id: req.params.id }).then((category) =>
    res.send(category)
  )
})

router.post('/', auth.verify, (req, res) => {
  const arg = {
    user: auth.decode(req.headers.authorization),
    name: req.body.name,
    type: req.body.type,
  }
  CategoryController.add(arg).then((result) => res.send(result))
})

router.put('/:id', auth.verify, (req, res) => {
  //obtain the URL parameter via req.params
  //identify whether the user who sent this request is AUTHORIZED to do so
  const arg = {
    categoryId: req.params.id,
    name: req.body.name,
    type: req.body.type,
    isRegistered: auth.decode(req.headers.authorization).isRegistered,
  }
  CategoryController.update(arg).then((result) => res.send(result))
})

router.delete('/:id', auth.verify, (req, res) => {
  const arg = {
    categoryId: req.params.id,
    isRegistered: auth.decode(req.headers.authorization).isRegistered,
  }
  CategoryController.archive(arg).then((result) => res.send(result))
})

module.exports = router
