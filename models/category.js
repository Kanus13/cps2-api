const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    isActive: {
    type: Boolean,
    default: true
  },
    createdOn: {
  	type: Date,
  	default: Date.now
  },
  users: [
  {
    userId: {
      type: String,
      required: [true, 'User ID is required']
    },
    createdOn: {
      type: Date,
      default: Date.now
    }
  }
  ]
})

module.exports = mongoose.model('category', categorySchema)