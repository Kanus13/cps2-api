const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'First name is required'],
  },
  lastName: {
    type: String,
    required: [true, 'Last name is required'],
  },
  mobileNumber: {
    type: String,
    // required: [true, 'Mobile number is required']
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
  },
  password: {
    type: String,
  },
  isRegistered: {
    type: Boolean,
    default: true,
  },
  balance: {
    type: Number,
    default: 0
  },
  loginType: {
    type: String,
    required: [true, 'Login type is required'],
  },
  avatar: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  },
  records: [
        {
            recordId: {
                type: String,
                required: [true, 'Record ID is required']
            },
            createdOn: {
                type: Date,
                default: Date.now
            }
        }
    ],
  categories: [
  {
    categoryId: {
      type: String,
      required: [true, 'Category ID is required']
    },
    createdOn: {
      type: Date,
      default: Date.now
    }
  }
  ]
})

module.exports = mongoose.model('user', userSchema)
