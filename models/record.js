const mongoose = require('mongoose')


const recordSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
  },
  createdOn: {
  	type: Date,
  	default: Date.now
  },
  isActive: {
    type: Boolean,
    default: true
  },
  users: [
  {
    userId: {
      type: String,
      required: [true, 'User ID is required']
    },
    createdOn: {
      type: Date,
      default: Date.now
    }
  }
  ]
})

module.exports = mongoose.model('record', recordSchema)
