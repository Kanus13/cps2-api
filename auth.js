const jwt = require('jsonwebtoken')
const secret = process.env.SECRET

//create JWT
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isRegistered: user.isRegistered
    } 
    return jwt.sign(data, secret, {})
}

//verify authenticity of JWT
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization

    if (typeof token !== 'undefined') {
        token = token.slice(7, token.length)

        return jwt.verify(token, secret, (err, data) => {
            return (err) ? res.send({ auth: 'failed' }) : next()
        })
    } else {
        return res.send({ auth: 'failed' })
    }
}

//decode JWT to get information from it (ie. some user info is encoded in the payload)
module.exports.decode = (token) => {//the token parameter is for passing in the actual token value as an argument
    if (typeof token !== 'undefined') {
        token = token.slice(7, token.length)
        //verify token first prior to decoding the contents of its payload
        return jwt.verify(token, secret, (err, data) => {
            //if verification results in error, do NOT decode
            return (err) ? null : jwt.decode(token, { complete: true }).payload
        })
    } else {
        return null
    }
}
